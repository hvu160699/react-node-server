const express = require('express');
const path = require('path');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const db = mongoose.connection

const productRouter = require('./routes/product');

const app = express();

const dbUrl = process.env.DATABASE_URL || 'mongodb+srv://hoangvu:HPLrIHxEI9EAziRZ@example-8o8jm.mongodb.net/test?retryWrites=true&w=majority'
mongoose.connect(dbUrl, { useNewUrlParser: true, useUnifiedTopology: true })
db.on('error', error => console.error(error))
db.once('open', () => console.log('Connected to Mongoose'))

app.use(cors())
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/product', productRouter);

module.exports = app;
