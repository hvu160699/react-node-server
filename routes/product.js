const express = require('express');
const router = express.Router();
const productService = require('../services/productServices');

router.route('/')
  .get(productService.getProducts)
  .post(productService.createProduct)

router.route('/:id')
  .get(productService.getProductByID)
  .put(productService.updateProductByID)
  .delete(productService.deleteProductByID)


module.exports = router;
